<?php

use Carbon\Carbon;

if (! function_exists('format_date')) {
    function format_date($date, $format = 'd F Y')
    {
        return $date ? Carbon::parse($date)->format($format) : null;
    }
}

if (! function_exists('format_gaji')) {
    function format_gaji($salary)
    {
        $gaji = (int) $salary;
        return $gaji ? number_format($gaji,2, ',' , '.'):null;
    }
}
