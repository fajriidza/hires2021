<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DataPegawaiExport implements FromView, WithStyles, ShouldAutoSize
{
    use Exportable;

    private $data;

    public function __construct(Object $data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('export.data-pegawai', [
            'data' => $this->data
        ]);
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle("A2:AF" . (count($this->data) + 2));
    }
}
