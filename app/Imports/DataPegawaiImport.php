<?php


namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\DataPegawai\Models\DataPegawai;
use Modules\DataPegawai\Services\DataPegawaiService;

class DataPegawaiImport implements ToModel, WithHeadingRow
{

    public $total = 0;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $service = new DataPegawaiService();
        $role = $service->getRole();
        if($role->name === 'Admin'){
            return new DataPegawai([
                'nip' => $row['nip'],
                'nik' => $row['nik'],
                'no_bpjs_kesehatan' => $row['no_bpjs_kesehatan'],
                'no_bpjs_tenaga_kerja' => $row['no_bpjs_tenaga_kerja'],
                'nama_karyawan' => $row['nama_karyawan'],
                'tempat_lahir' => $row['tempat_lahir'],
                'tanggal_lahir' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_lahir'])),
                'pendidikan_terakhir' => $row['pendidikan_terakhir'],
                'alamat' => $row['alamat'],
                'jabatan' => $row['jabatan'],
                'divisi' => $row['divisi'],
                'perusahaan' => $row['perusahaan'],
                'tanggal_masuk' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_masuk'])),
                'mulai_kontrak' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['mulai_kontrak'])),
                'habis_kontrak' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['habis_kontrak'])),
                'catatan' => $row['catatan'],
                'keluarga' => $row['keluarga'],
                'gaji' => $row['gaji'],
                'status' => 'Aktif'
            ]);
        }else{
            if($row['perusahaan'] === $role->name){
                return new DataPegawai([
                    'nip' => $row['nip'],
                    'nik' => $row['nik'],
                    'no_bpjs_kesehatan' => $row['no_bpjs_kesehatan'],
                    'no_bpjs_tenaga_kerja' => $row['no_bpjs_tenaga_kerja'],
                    'nama_karyawan' => $row['nama_karyawan'],
                    'tempat_lahir' => $row['tempat_lahir'],
                    'tanggal_lahir' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_lahir'])),
                    'pendidikan_terakhir' => $row['pendidikan_terakhir'],
                    'alamat' => $row['alamat'],
                    'jabatan' => $row['jabatan'],
                    'divisi' => $row['divisi'],
                    'perusahaan' => $row['perusahaan'],
                    'tanggal_masuk' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_masuk'])),
                    'mulai_kontrak' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['mulai_kontrak'])),
                    'habis_kontrak' => \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['habis_kontrak'])),
                    'catatan' => $row['catatan'],
                    'keluarga' => $row['keluarga'],
                    'gaji' => $row['gaji'],
                    'status' => 'Aktif'
                ]);
            }
        }
    }

}
