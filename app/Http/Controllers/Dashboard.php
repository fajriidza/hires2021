<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\DataPegawai\Services\DataPegawaiService;

class Dashboard extends Controller
{
    /**
     * @var DataPegawaiService
     */
    private $service;

    public function __construct(DataPegawaiService $service)
    {
        $this->service = $service;
    }
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $userRole = $this->service->getRole();
        $roles = DB::table('acl_roles')->whereNotIn('name',['Admin'])->get();
        $data = [];
        if($userRole->name === 'Admin') {
            foreach ($roles as $role) {
                $data[$role->name] = DB::table('data_pegawai')->where('perusahaan', $role->name)->count();
            }
        }else{
            $data[$userRole->name] = DB::table('data_pegawai')->where('perusahaan', $userRole->name)->count();
        }
        return view('dashboard',compact('data','roles','userRole'));
    }
}
