<?php

return [

    /*
     * Example Menu
     */
    '' => [
        'menu' => [
            'Dashboard'  => [
                'url'  => 'dashboard',
                'data' => ['icon' => 'home'],
            ],
        ],
    ],
    'Data' => [
        'menu' => [
            'Data Pegawai'      => [
                'url'  => 'modules/data-pegawai',
                'data' => ['icon' => 'user'],
                ],
            'Import Data Excel/Offline'  => [
                'url'  => 'modules/import-data-pegawai',
                'data' => ['icon' => 'upload'],
                ],
            'Export Data Pegawai'  => [
                'url'  => 'modules/export-data-pegawai',
                'data' => ['icon' => 'download'],
            ],
            'Daftar Pegawai Habis Kontrak'  => [
                'url'  => 'modules/pegawai-habis-kontrak',
                'data' => ['icon' => 'address book'],
            ],
            ]
        ],
    'KEHADIRAN' => [
    'menu' => [
        'Absensi'  => [
            'url'  => '#',
            'data' => ['icon' => 'book'],
        ],
        'Cuti'  => [
            'url'  => 'modules/cuti',
            'data' => ['icon' => 'book'],
        ],
        ],
    ],
    'Bonus Pegawai' => [
        'menu' => [
            'Bonus'  => [
                'url'  => '#',
                'data' => ['icon' => 'trophy'],
            ],
        ],
    ],
    'Master Data' => [
        'menu' => [
            'Perusahaan'  => [
                'url'  => 'modules/perusahaan',
                'data' => ['icon' => 'building'],
            ],
            'Divisi'  => [
                'url'  => 'modules/divisi',
                'data' => ['icon' => 'users'],
            ],
        ],
        'data' => ['permission' => 'Admin'],
    ]
];
