new AutoNumeric('.miliar-auto-numeric', {
    allowDecimalPadding: false,
    decimalCharacter: ",",
    digitGroupSeparator: ".",
    unformatOnSubmit: true
});