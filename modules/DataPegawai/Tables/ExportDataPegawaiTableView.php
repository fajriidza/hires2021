<?php

namespace Modules\DataPegawai\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\DataPegawai\Models\DataPegawai;

class ExportDataPegawaiTableView extends TableView
{
    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nip')->sortable(),
            Text::make('nik')->sortable(),
            Text::make('nama_karyawan')->sortable(),
            Text::make('jabatan')->sortable(),
            Text::make('divisi')->sortable(),
            Text::make('perusahaan')->sortable(),
            ['header' => 'status', 'raw' => function($row){
                $label = $row->status === "Aktif"?'<a class="ui green label">'.$row->status.'</a>' : '<a class="ui red label">'.$row->status.'</a>';
                return $label;
            },'sortable' => 'status'],
            Raw::make(function ($row){
                return format_date($row->habis_kontrak);
            },'Habis Kontrak')->sortable(),
            Raw::make(function ($row){
                if(!is_null($row->media->first()))
                    return "<img  height='100' src=".$row->media->first()->getFullUrl().">";
                else{
                    return '-';
                }
            },'Foto'),
        ];
    }
}
