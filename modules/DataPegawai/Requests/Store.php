<?php

namespace Modules\DataPegawai\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip' => ['required'],
            'no_bpjs_kesehatan' => ['required'],
            'no_bpjs_tenaga_kerja' => ['required'],
            'nama_karyawan' => ['required'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'pendidikan_terakhir' => ['required'],
            'alamat' => ['required'],
            'divisi' => ['required'],
            'perusahaan' => ['required'],
            'tanggal_masuk' => ['required'],
            'mulai_kontrak' => ['required'],
            'habis_kontrak' => ['required'],
            'catatan' => ['required'],
            'keluarga' => ['required'],
            'gaji' => ['required'],
            'foto' => ['required']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
