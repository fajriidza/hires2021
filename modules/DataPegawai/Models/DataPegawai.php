<?php

namespace Modules\DataPegawai\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;
use Modules\Divisi\Models\Divisi;
use Modules\Perusahaan\Models\Perusahaan;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class DataPegawai extends Model implements HasMedia
{
    use AutoSearch, AutoSort, AutoFilter, InteractsWithMedia;

    protected $table = 'data_pegawai';

    protected $guarded = [];

    protected $searchableColumns = ["nip","no_bpjs_tenaga_kerja","no_bpjs_kesehatan","gaji", "nama_karyawan", "tempat_lahir", "tanggal_lahir", "pendidikan_terakhir", "alamat", "divisi", "perusahaan", "tanggal_masuk", "mulai_kontrak", "habis_kontrak", "catatan", "keluarga",];
    protected $fillable = ["nip","nik","no_bpjs_tenaga_kerja","no_bpjs_kesehatan","gaji", "nama_karyawan", "tempat_lahir","status", "jabatan", "tanggal_lahir", "pendidikan_terakhir", "alamat", "divisi", "perusahaan", "tanggal_masuk", "mulai_kontrak", "habis_kontrak", "catatan", "keluarga",];
    protected $dates = ['tanggal_lahir','tanggal_masuk','mulai_kontrak','habis_kontrak'];

    public function divisi()
    {
        return $this->belongsTo(Divisi::class);
    }

    public function perusahaan()
    {
        return $this->belongsTo(Perusahaan::class);
    }

}
