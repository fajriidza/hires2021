<?php

use Modules\DataPegawai\Controllers\DataPegawaiController;

$router->group(
    [
        'prefix' => config('modules.data-pegawai.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.data-pegawai.route.middleware'),
    ],
    function ($router) {
        $router->resource('data-pegawai', DataPegawaiController::class);
        $router->get('export-data-pegawai','Modules\DataPegawai\Controllers\ExportDataPegawaiController@index')
            ->name('export-data-pegawai.index');
        $router->get('export-data-pegawai/download','Modules\DataPegawai\Controllers\ExportDataPegawaiController')
            ->name('export-data-pegawai.download');
        $router->get('import-data-pegawai','Modules\DataPegawai\Controllers\ImportDataPegawaiController@index')
            ->name('import-data-pegawai.index');
        $router->post('import-data-pegawai','Modules\DataPegawai\Controllers\ImportDataPegawaiController@import')
            ->name('import-data-pegawai.upload');
    }
);
