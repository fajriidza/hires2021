@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="DataPegawai">
        <x-item>
            <x-link label="Download" target="_blank" class="green" icon="download" url="{{route('modules::export-data-pegawai.download')}}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
