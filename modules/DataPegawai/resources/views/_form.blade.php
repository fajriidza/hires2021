@if(isset($dataPegawai))
    @if(!is_null($dataPegawai->media->first()))
    <div class="field center">
            <img class="ui centered image"  height="100" width="100" src="{{$dataPegawai->media->first()->getFullUrl()}}">
    </div>
    @endif
 @endif
{!! form()->text('nip')->label('NIP')->required() !!}
{!! form()->text('nik')->label('NIK')->required() !!}
    {!! form()->text('no_bpjs_tenaga_kerja')->label('No BPJS Tenaga Kerja')->required() !!}
    {!! form()->text('no_bpjs_kesehatan')->label('No BPJS Kesehatan')->required() !!}
	{!! form()->text('nama_karyawan')->label('Nama Karyawan')->required() !!}
	{!! form()->text('tempat_lahir')->label('Tempat Lahir')->required() !!}
	{!! form()->datepicker('tanggal_lahir',null,'d M Y')->label('Tanggal Lahir')->required() !!}
	{!! form()->text('pendidikan_terakhir')->label('Pendidikan Terakhir')->required() !!}
	{!! form()->textarea('alamat')->label('Alamat')->required() !!}
    {!! form()->text('jabatan')->label('Jabatan')->required() !!}
	{!! form()->dropdown('divisi',$divisi)->placeholder('-- Pilih Divisi --')->label('Divisi')->required() !!}
	{!! form()->dropdown('perusahaan',$perusahaan)->placeholder('-- Pilih Perusahaan --')->label('Perusahaan')->required() !!}
	{!! form()->datepicker('tanggal_masuk',null,'d M Y')->label('Tanggal Masuk')->required() !!}
	{!! form()->datepicker('mulai_kontrak',null,'d M Y')->label('Mulai Kontrak')->required() !!}
	{!! form()->datepicker('habis_kontrak',null,'d M Y')->label('Habis Kontrak')->required() !!}
	{!! form()->textarea('catatan')->label('Catatan')->required() !!}
	{!! form()->textarea('keluarga')->label('Keluarga')->required() !!}
    {!! form()->text('gaji')->addClass('decimalNumber')->label('Gaji')->required() !!}
    {!! form()->dropdown('status',['Aktif' => 'Aktif', 'Tidak Aktif' => 'Tidak Aktif'])->placeholder('-- Status --')->label('Status')->required() !!}
    @if(isset($dataPegawai))
        <div class="field">
            <label for="">Unggah Foto Baru Pegawai</label>
            {!! form()->file('foto')->attribute('accept', '.jpg,.jpeg,.png') !!}
        </div>
    @else
        <div class="field">
            <label for="">Foto Pegawai</label>
            {!! form()->file('foto')->attribute('accept', '.jpg,.jpeg,.png')->required() !!}
        </div>
    @endif
    {!! form()->action([
        form()->submit('Simpan'),
        form()->link('Batal', route('modules::data-pegawai.index'))
    ]) !!}

@push('script')
    <script src="{{ asset('lib/auto-numeric/autonumeric.js') }}"></script>

    <script>
        new AutoNumeric('.decimalNumber', {
            decimalCharacter: ",",
            digitGroupSeparator: ".",
            unformatOnSubmit: true
        });
    </script>
@endpush
