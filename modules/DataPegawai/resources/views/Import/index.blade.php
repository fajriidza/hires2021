@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Import Data Pegawai">

    </x-titlebar>

    <x-panel>
        {!! form()->post(route('modules::import-data-pegawai.upload'))->multipart() !!}
        {!! form()->file('file_import')->label('Import Excel')->attribute('accept', '.xlsx, .xls')->required()!!}
            <button type="submit" class="ui button primary green"><i class="upload icon"></i> Import Excel</button>
        <x-link label="Download Template" class="green" icon="download" url="{{asset('/TemplateExcel/template.xlsx')}}"></x-link>

        {!! form()->close() !!}

        <div class="ui grid stackable">
            <div class="ten wide column">
                <div class="ui divider hidden"></div>
                <div class="ui accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            Cara Import Data Excel
                        </div>
                        <div class="content">
                            <p class="transition hidden">1. Download terlebih dahulu Template Excel.<br>
                            2. Isikan data pada template, format untuk tanggal yaitu Tahun-Bulan-hari. Contoh 2020-08-01<br>
                            3. Pastikan nama Divisi dan Perusahan sama seperti di Aplikasi. Huruf besar dan Kecil mempengaruhi.<br>
                            4. Gaji diisi tanpa tanda baca. Contoh 2500000</p>
                        </div>
                </div>
            </div>
            <div class="six wide column">
                <img src="{{ asset('img/faq.svg') }}" class="ui image centered" alt="">
            </div>
        </div>
        </div>
    </x-panel>
@stop
@push('script')
    <script>
        $( document ).ready(function() {
            $('.ui.accordion')
                .accordion();
        });

    </script>
@endpush
