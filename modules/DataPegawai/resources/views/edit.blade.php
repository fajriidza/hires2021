@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::data-pegawai.index') }}"></x-backlink>

    <x-panel title="Edit DataPegawai">
        {!! form()->bind($dataPegawai)->put(route('modules::data-pegawai.update', $dataPegawai->getKey()))->autocomplete('off')->horizontal()->multipart() !!}
        @include('data-pegawai::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
