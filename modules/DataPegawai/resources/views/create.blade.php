@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::data-pegawai.index') }}"></x-backlink>

    <x-panel title="Tambah DataPegawai">
        {!! form()->post(route('modules::data-pegawai.store'))->autocomplete('off')->horizontal()->multipart() !!}
        @include('data-pegawai::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
