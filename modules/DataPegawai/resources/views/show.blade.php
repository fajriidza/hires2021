@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::data-pegawai.index') }}"></x-backlink>

    <x-panel title="Detil DataPegawai">
        @if(!is_null($dataPegawai->media->first()))
            <div class="field center">
                <img class="ui centered image"  height="100" width="100" src="{{$dataPegawai->media->first()->getFullUrl()}}">
            </div>
       @endif

        <table class="ui table definition">
            <tr><td>Status</td><td>{!!$dataPegawai->status === "Aktif"?'<a class="ui green label">'.$dataPegawai->status.'</a>' : '<a class="ui red label">'.$dataPegawai->status.'</a>' !!} </td></tr>
        <tr><td>NIP</td><td>{{ $dataPegawai->nip }}</td></tr>
            <tr><td>NIK</td><td>{{ $dataPegawai->nik }}</td></tr>
        <tr><td>No BPJS Kesehatan</td><td>{{ $dataPegawai->no_bpjs_kesehatan }}</td></tr>
        <tr><td>No BPJS Tenaga Kerja</td><td>{{ $dataPegawai->no_bpjs_tenaga_kerja }}</td></tr>
        <tr><td>Nama Karyawan</td><td>{{ $dataPegawai->nama_karyawan }}</td></tr>
        <tr><td>Tempat Lahir</td><td>{{ $dataPegawai->tempat_lahir }}</td></tr>
        <tr><td>Tanggal Lahir</td><td>{{ format_date($dataPegawai->tanggal_lahir) }}</td></tr>
        <tr><td>Pendidikan Terakhir</td><td>{{ $dataPegawai->pendidikan_terakhir }}</td></tr>
        <tr><td>Alamat</td><td>{{ $dataPegawai->alamat }}</td></tr>
        <tr><td>Jabatan</td><td>{{ $dataPegawai->jabatan }}</td></tr>
        <tr><td>Divisi</td><td>{{ $dataPegawai->divisi }}</td></tr>
        <tr><td>Perusahaan</td><td>{{ $dataPegawai->perusahaan }}</td></tr>
        <tr><td>Tanggal Masuk</td><td>{{ format_date($dataPegawai->tanggal_masuk) }}</td></tr>
        <tr><td>Mulai Kontrak</td><td>{{ format_date($dataPegawai->mulai_kontrak) }}</td></tr>
        <tr><td>Habis Kontrak</td><td>{{ format_date($dataPegawai->habis_kontrak) }}</td></tr>
        <tr><td>Catatan</td><td>{{ $dataPegawai->catatan }}</td></tr>
        <tr><td>Keluarga</td><td>{{ $dataPegawai->keluarga }}</td></tr>
        <tr><td>Gaji</td><td>{{ format_gaji($dataPegawai->gaji) }}</td></tr>
        </table>
    </x-panel>

@stop
