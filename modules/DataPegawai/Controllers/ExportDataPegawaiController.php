<?php

namespace Modules\DataPegawai\Controllers;

use App\Exports\DataPegawaiExport;
use Illuminate\Routing\Controller;
use Modules\DataPegawai\Models\DataPegawai;
use Modules\DataPegawai\Services\DataPegawaiService;
use Modules\DataPegawai\Tables\ExportDataPegawaiTableView;

class ExportDataPegawaiController extends Controller
{

    public function __invoke()
    {
        $service = new DataPegawaiService();
        $role = $service->getRole();
        if($role->name === 'Admin'){
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()->autoSearch(request('search'))->get();

        }else{
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()
                ->autoSearch(request('search'))
                ->where('perusahaan',$role->name)
                ->get();
        }
        return (new DataPegawaiExport($data))->download( 'Data Pegawai.xlsx');
    }

    public function index()
    {
        $service = new DataPegawaiService();
        $role = $service->getRole();
        if($role->name === 'Admin'){
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()->autoSearch(request('search'))->paginate();

        }else{
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()
                ->autoSearch(request('search'))
                ->where('perusahaan',$role->name)
                ->paginate();
        }
        return ExportDataPegawaiTableView::make($data)->view('data-pegawai::Export.index');
    }
}
