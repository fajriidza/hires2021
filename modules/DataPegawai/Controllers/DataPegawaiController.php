<?php

namespace Modules\DataPegawai\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\DataPegawai\Requests\Store;
use Illuminate\Http\Request;
use Modules\DataPegawai\Requests\Update;
use Modules\DataPegawai\Models\DataPegawai;
use Modules\DataPegawai\Services\DataPegawaiService;
use Modules\DataPegawai\Tables\DataPegawaiTableView;

class DataPegawaiController extends Controller
{
    /**
     * @var DataPegawaiService
     */
    private $service;

    public function __construct(DataPegawaiService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $role = $this->service->getRole();
        if($role->name === 'Admin'){
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()->autoSearch(request('search'))->paginate();

        }else{
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()
                ->autoSearch(request('search'))
                ->where('perusahaan',$role->name)
                ->paginate();
        }
        return DataPegawaiTableView::make($data)->view('data-pegawai::index');
    }

    public function create()
    {
        $perusahaan = $this->service->getPerusahaan();
        $divisi = $this->service->getDivisi();
        return view('data-pegawai::create', compact('perusahaan','divisi'));
    }

    public function store(Request $request)
    {

        $dataPegawai = DataPegawai::create($request->all());
        $dataPegawai->addMedia($request->foto)
            ->toMediaCollection('fotoPegawai');

        return redirect()->route('modules::data-pegawai.index')->withSuccess('Data Pegawai saved');
    }

    public function show(DataPegawai $dataPegawai)
    {
        return view('data-pegawai::show', compact('dataPegawai'));
    }

    public function edit(DataPegawai $dataPegawai)
    {
        $perusahaan = $this->service->getPerusahaan();
        $divisi = $this->service->getDivisi();
        return view('data-pegawai::edit', compact('dataPegawai','perusahaan','divisi'));
    }

    public function update(Request $request, DataPegawai $dataPegawai)
    {
        if ($request->foto) {
            $dataPegawai->media->each->delete();
            $dataPegawai->addMedia($request->foto)->toMediaCollection('fotoPegawai');
        }
        $dataPegawai->update($request->all());

        return redirect()->route('modules::data-pegawai.index')->withSuccess('Data Pegawai saved');
    }

    public function destroy(DataPegawai $dataPegawai)
    {
        $dataPegawai->delete();

        return redirect()->back()->withSuccess('Data Pegawai deleted');
    }
}
