<?php

namespace Modules\DataPegawai\Controllers;

use App\Exports\DataPegawaiExport;
use App\Imports\DataPegawaiImport;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Modules\DataPegawai\Models\DataPegawai;
use Modules\DataPegawai\Tables\ExportDataPegawaiTableView;

class ImportDataPegawaiController extends Controller
{

    public function __invoke()
    {
        $data = DataPegawai::with('divisi','perusahaan')->get();
        return (new DataPegawaiExport($data))->download( 'Data Pegawai.xlsx');
    }

    public function index()
    {
        return view('data-pegawai::Import.index');
    }

    public function import(Request $request)
    {
        $file = $request->file('file_import');

        $nama_file = rand().$file->getClientOriginalName();

        // upload ke folder file_siswa di dalam folder public
        $file->move('data_pegawai',$nama_file);

        // import data
        try {
            Excel::import(new DataPegawaiImport, public_path('/data_pegawai/'.$nama_file));
            return redirect()->route('modules::data-pegawai.index')->withSuccess('Data berhasil di import');
        } catch (\Exception $e) {
            return redirect()->route('modules::import-data-pegawai.index')->withErrors('Format tidak sesuai!');
        }

    }
}
