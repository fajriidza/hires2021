<?php

namespace Modules\DataPegawai\Services;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Divisi\Models\Divisi;
use Modules\Perusahaan\Models\Perusahaan;

class DataPegawaiService
{
    public function getPerusahaan()
    {
        $perusahaan = Perusahaan::all();

        return $perusahaan->pluck('nama_perusahaan', 'nama_perusahaan');
    }

    public function getDivisi()
    {
        $divisi = Divisi::all();

        return $divisi->pluck('divisi', 'divisi');
    }

    public function getRole()
    {
        $userId = Auth::user()->id;
       return  DB::table('acl_roles')
            ->join('acl_role_user','acl_roles.id','=','acl_role_user.role_id')
            ->where('acl_role_user.user_id',$userId)->first();
    }

}
