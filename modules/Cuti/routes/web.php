<?php

use Modules\Cuti\Controllers\CutiController;

$router->group(
    [
        'prefix' => config('modules.cuti.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.cuti.route.middleware'),
    ],
    function ($router) {
        $router->resource('cuti', CutiController::class);
    }
);
