<?php

namespace Modules\Cuti\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Cuti extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'cuti';

    protected $guarded = [];

    protected $searchableColumns = ["perusahaan", "divisi", "nama", "jabatan", "jumlah_cuti", "tanggal_mulai_cuti","sisa_cuti", "tanggal_masuk", "alasan", "potong_cuti", "tidak_potong_cuti", "sisa_setelah_potong_cuti", "potong_gaji", "catatan",];
     protected $fillable = ["perusahaan", "divisi", "nama", "jabatan", "jumlah_cuti", "tanggal_mulai_cuti","sisa_cuti", "tanggal_masuk", "alasan", "potong_cuti", "tidak_potong_cuti", "sisa_setelah_potong_cuti", "potong_gaji", "catatan",];
}
