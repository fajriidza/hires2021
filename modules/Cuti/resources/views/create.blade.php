@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::cuti.index') }}"></x-backlink>

    <x-panel title="Tambah Cuti">
        {!! form()->post(route('modules::cuti.store'))->horizontal()->multipart() !!}
        @include('cuti::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
