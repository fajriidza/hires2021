{!! form()->dropdown('perusahaan',$perusahaan)->placeholder('-- Pilih Perusahaan --')->label('Perusahaan')->required() !!}
{!! form()->dropdown('divisi',$divisi)->placeholder('-- Pilih Divisi --')->label('Divisi')->required() !!}
	{!! form()->text('nama')->label('Nama')->required() !!}
	{!! form()->text('jabatan')->label('Jabatan')->required() !!}
	{!! form()->number('jumlah_cuti')->label('Jumlah Cuti')->placeholder('Hari')->required() !!}
    {!! form()->number('sisa_cuti')->label('Sisa Cuti')->placeholder('Hari')->required() !!}
	{!! form()->datepicker('tanggal_mulai_cuti')->label('Tanggal Mulai Cuti')->required() !!}
	{!! form()->datepicker('tanggal_masuk')->label('Tanggal Masuk')->required() !!}
	{!! form()->textarea('alasan')->label('Alasan')->required() !!}
	{!! form()->number('potong_cuti')->label('Potong Cuti')->placeholder('Hari')->required() !!}
	{!! form()->number('tidak_potong_cuti')->label('Tidak Potong Cuti')->placeholder('Hari')->required() !!}
	{!! form()->Number('sisa_setelah_potong_cuti')->label('Sisa Setelah Potong')->placeholder('Hari')->required() !!}
	{!! form()->Number('potong_gaji')->label('Potong Gaji')->placeholder('Hari')->required() !!}
	{!! form()->textarea('catatan')->label('Catatan')->required() !!}
{!! form()->action([
    form()->submit('Simpan'),
    form()->link('Batal', route('modules::cuti.index'))
]) !!}
