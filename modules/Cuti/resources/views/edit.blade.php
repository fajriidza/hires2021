@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::cuti.index') }}"></x-backlink>

    <x-panel title="Edit Cuti">
        {!! form()->bind($cuti)->put(route('modules::cuti.update', $cuti->getKey()))->horizontal()->multipart() !!}
        @include('cuti::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
