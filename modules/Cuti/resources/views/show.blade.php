@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::cuti.index') }}"></x-backlink>

    <x-panel title="Detil Cuti">
        <table class="ui table definition">
        <tr><td>Perusahaan</td><td>{{ $cuti->perusahaan }}</td></tr>
        <tr><td>Divisi</td><td>{{ $cuti->divisi }}</td></tr>
        <tr><td>Nama</td><td>{{ $cuti->nama }}</td></tr>
        <tr><td>Jabatan</td><td>{{ $cuti->jabatan }}</td></tr>
        <tr><td>Jumlah Cuti</td><td>{{ $cuti->jumlah_cuti }}</td></tr>
        <tr><td>Sisa Cuti</td><td>{{ $cuti->sisa_cuti }}</td></tr>
        <tr><td>Tanggal Mulai Cuti</td><td>{{ format_date($cuti->tanggal_mulai_cuti) }}</td></tr>
        <tr><td>Tanggal Masuk</td><td>{{ format_date($cuti->tanggal_masuk) }}</td></tr>
        <tr><td>Alasan</td><td>{{ $cuti->alasan }}</td></tr>
        <tr><td>Potong Cuti</td><td>{{ $cuti->potong_cuti }}</td></tr>
        <tr><td>Tidak Potong Cuti</td><td>{{ $cuti->tidak_potong_cuti }}</td></tr>
        <tr><td>Sisa Setelah Potong Cuti</td><td>{{ $cuti->sisa_setelah_potong_cuti }}</td></tr>
        <tr><td>Potong Gaji</td><td>{{ $cuti->potong_gaji }}</td></tr>
        <tr><td>Catatan</td><td>{{ $cuti->catatan }}</td></tr>
        </table>
    </x-panel>

@stop
