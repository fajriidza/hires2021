@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Cuti">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::cuti.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
