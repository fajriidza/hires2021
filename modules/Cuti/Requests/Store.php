<?php

namespace Modules\Cuti\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'perusahaan' => ['required'],
            'divisi' => ['required'],
            'nama' => ['required'],
            'jabatan' => ['required'],
            'jumlah_cuti' => ['required'],
            'sisa_cuti' => ['required'],
            'tanggal_mulai_cuti' => ['required'],
            'tanggal_masuk' => ['required'],
            'alasan' => ['required'],
            'potong_cuti' => ['required'],
            'tidak_potong_cuti' => ['required'],
            'sisa_setelah_potong_cuti' => ['required'],
            'potong_gaji' => ['required'],
            'catatan' => ['required'],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
