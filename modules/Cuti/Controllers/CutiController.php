<?php

namespace Modules\Cuti\Controllers;

use Illuminate\Routing\Controller;
use Modules\Cuti\Requests\Store;
use Modules\Cuti\Requests\Update;
use Modules\Cuti\Models\Cuti;
use Modules\Cuti\Services\CutiService;
use Modules\Cuti\Tables\CutiTableView;

class CutiController extends Controller
{
    /**
     * @var CutiService
     */
    private $service;

    public function __construct(CutiService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        return CutiTableView::make()->view('cuti::index');
    }

    public function create()
    {
        $perusahaan = $this->service->getPerusahaan();
        $divisi = $this->service->getDivisi();
        return view('cuti::create', compact('perusahaan','divisi'));
    }

    public function store(Store $request)
    {
        Cuti::create($request->validated());

        return redirect()->route('modules::cuti.index')->withSuccess('Cuti saved');
    }

    public function show(Cuti $cuti)
    {
        return view('cuti::show', compact('cuti'));
    }

    public function edit(Cuti $cuti)
    {
        $perusahaan = $this->service->getPerusahaan();
        $divisi = $this->service->getDivisi();
        return view('cuti::edit', compact('cuti','perusahaan','divisi'));
    }

    public function update(Update $request, Cuti $cuti)
    {
        $cuti->update($request->validated());

        return redirect()->route('modules::cuti.index')->withSuccess('Cuti saved');
    }

    public function destroy(Cuti $cuti)
    {
        $cuti->delete();

        return redirect()->back()->withSuccess('Cuti deleted');
    }
}
