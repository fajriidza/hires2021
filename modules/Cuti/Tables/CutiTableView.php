<?php

namespace Modules\Cuti\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\Raw;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Cuti\Models\Cuti;

class CutiTableView extends TableView
{
    public function source()
    {
        return Cuti::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama')->sortable(),
            Text::make('perusahaan')->sortable(),
            Text::make('divisi')->sortable(),
            Text::make('jabatan')->sortable(),
            Text::make('jumlah_cuti')->sortable(),
            Raw::make(function ($row){
                return format_date($row->tanggal_mulai_cuti);
            },'Tanggal Mulai Cuti')->sortable(),
            Raw::make(function ($row){
                return format_date($row->tanggal_masuk);
            },'Tanggal Masuk')->sortable(),
            RestfulButton::make('modules::cuti'),
        ];
    }
}
