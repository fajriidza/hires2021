<?php

namespace Modules\PegawaiHabisKontrak\Providers;

use Laravolt\Support\Base\BaseServiceProvider;

class PegawaiHabisKontrakServiceProvider extends BaseServiceProvider
{
    public function getIdentifier()
    {
        return 'pegawai-habis-kontrak';
    }

    public function register()
    {
        $file = $this->packagePath("config/{$this->getIdentifier()}.php");
        $this->mergeConfigFrom($file, "modules.{$this->getIdentifier()}");
        $this->publishes([$file => config_path("modules/{$this->getIdentifier()}.php")], 'config');

        $this->config = collect(config("modules.{$this->getIdentifier()}"));
    }

}
