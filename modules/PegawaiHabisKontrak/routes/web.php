<?php

use Modules\PegawaiHabisKontrak\Controllers\PegawaiHabisKontrakController;

$router->group(
    [
        'prefix' => config('modules.pegawai-habis-kontrak.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.pegawai-habis-kontrak.route.middleware'),
    ],
    function ($router) {
        $router->resource('pegawai-habis-kontrak', PegawaiHabisKontrakController::class)
            ->only('index','show');
    }
);
