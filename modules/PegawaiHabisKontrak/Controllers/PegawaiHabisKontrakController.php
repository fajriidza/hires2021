<?php

namespace Modules\PegawaiHabisKontrak\Controllers;

use Illuminate\Routing\Controller;
use Modules\DataPegawai\Models\DataPegawai;
use Modules\DataPegawai\Services\DataPegawaiService;
use Modules\PegawaiHabisKontrak\Tables\PegawaiHabisKontrakTableView;

class PegawaiHabisKontrakController extends Controller
{

    /**
     * @var DataPegawaiService
     */
    private $service;

    public function __construct(DataPegawaiService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $role = $this->service->getRole();
        if($role->name === 'Admin'){
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()->autoSearch(request('search'))
                ->where('habis_kontrak','<=',now())
                ->paginate();

        }else{
            $data = DataPegawai::autoSort()->with('divisi','perusahaan')->latest()
                ->autoSearch(request('search'))
                ->where('perusahaan',$role->name)
                ->where('habis_kontrak','<=',now())
                ->paginate();
        }
        return PegawaiHabisKontrakTableView::make($data)->view('pegawai-habis-kontrak::index');
    }
    public function show(DataPegawai $pegawai_habis_kontrak)
    {
        $dataPegawai = $pegawai_habis_kontrak;
        return view('pegawai-habis-kontrak::show', compact('dataPegawai'));
    }

}
