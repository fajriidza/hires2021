<?php

namespace Modules\Perusahaan\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Perusahaan\Models\Perusahaan;

class PerusahaanTableView extends TableView
{
    public function source()
    {
        return Perusahaan::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('nama_perusahaan')->sortable(),
            Text::make('alamat')->sortable(),
            RestfulButton::make('modules::perusahaan')->except('view'),
        ];
    }
}
