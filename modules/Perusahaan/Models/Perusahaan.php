<?php

namespace Modules\Perusahaan\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Perusahaan extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'perusahaan';

    protected $guarded = [];

    protected $searchableColumns = ["nama_perusahaan", "alamat",];
}
