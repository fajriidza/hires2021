<?php

use Modules\Perusahaan\Controllers\PerusahaanController;

$router->group(
    [
        'prefix' => config('modules.perusahaan.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.perusahaan.route.middleware'),
    ],
    function ($router) {
        $router->resource('perusahaan', PerusahaanController::class);
    }
);
