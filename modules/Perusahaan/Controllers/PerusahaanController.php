<?php

namespace Modules\Perusahaan\Controllers;

use Illuminate\Routing\Controller;
use Modules\Perusahaan\Requests\Store;
use Modules\Perusahaan\Requests\Update;
use Modules\Perusahaan\Models\Perusahaan;
use Modules\Perusahaan\Tables\PerusahaanTableView;

class PerusahaanController extends Controller
{
    public function index()
    {
        return PerusahaanTableView::make()->view('perusahaan::index');
    }

    public function create()
    {
        return view('perusahaan::create');
    }

    public function store(Store $request)
    {
        Perusahaan::create($request->validated());

        return redirect()->route('modules::perusahaan.index')->withSuccess('Perusahaan saved');
    }

    public function show(Perusahaan $perusahaan)
    {
        return view('perusahaan::show', compact('perusahaan'));
    }

    public function edit(Perusahaan $perusahaan)
    {
        return view('perusahaan::edit', compact('perusahaan'));
    }

    public function update(Update $request, Perusahaan $perusahaan)
    {
        $perusahaan->update($request->validated());

        return redirect()->route('modules::perusahaan.index')->withSuccess('Perusahaan saved');
    }

    public function destroy(Perusahaan $perusahaan)
    {
        $perusahaan->delete();

        return redirect()->back()->withSuccess('Perusahaan deleted');
    }
}
