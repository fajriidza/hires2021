@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::perusahaan.index') }}"></x-backlink>

    <x-panel title="Edit Perusahaan">
        {!! form()->bind($perusahaan)->put(route('modules::perusahaan.update', $perusahaan->getKey()))->horizontal()->multipart() !!}
        @include('perusahaan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
