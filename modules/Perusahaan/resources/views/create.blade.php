@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::perusahaan.index') }}"></x-backlink>

    <x-panel title="Tambah Perusahaan">
        {!! form()->post(route('modules::perusahaan.store'))->horizontal()->multipart() !!}
        @include('perusahaan::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
