@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::perusahaan.index') }}"></x-backlink>

    <x-panel title="Detil Perusahaan">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $perusahaan->id }}</td></tr>
        <tr><td>Nama Perusahaan</td><td>{{ $perusahaan->nama_perusahaan }}</td></tr>
        <tr><td>Alamat</td><td>{{ $perusahaan->alamat }}</td></tr>
        <tr><td>Created At</td><td>{{ $perusahaan->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $perusahaan->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
