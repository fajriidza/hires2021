@extends('laravolt::layouts.app')

@section('content')


    <x-titlebar title="Divisi">
        <x-item>
            <x-link label="Tambah" icon="plus" url="{{ route('modules::divisi.create') }}"></x-link>
        </x-item>
    </x-titlebar>

    {!! $table !!}
@stop
