@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::divisi.index') }}"></x-backlink>

    <x-panel title="Detil Divisi">
        <table class="ui table definition">
        <tr><td>Id</td><td>{{ $divisi->id }}</td></tr>
        <tr><td>Divisi</td><td>{{ $divisi->divisi }}</td></tr>
        <tr><td>Created At</td><td>{{ $divisi->created_at }}</td></tr>
        <tr><td>Updated At</td><td>{{ $divisi->updated_at }}</td></tr>
        </table>
    </x-panel>

@stop
