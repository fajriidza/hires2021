@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::divisi.index') }}"></x-backlink>

    <x-panel title="Edit Divisi">
        {!! form()->bind($divisi)->put(route('modules::divisi.update', $divisi->getKey()))->horizontal()->multipart() !!}
        @include('divisi::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
