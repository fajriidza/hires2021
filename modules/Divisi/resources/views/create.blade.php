@extends('laravolt::layouts.app')

@section('content')

    <x-backlink url="{{ route('modules::divisi.index') }}"></x-backlink>

    <x-panel title="Tambah Divisi">
        {!! form()->post(route('modules::divisi.store'))->horizontal()->multipart() !!}
        @include('divisi::_form')
        {!! form()->close() !!}
    </x-panel>

@stop
