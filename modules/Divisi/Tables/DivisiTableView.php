<?php

namespace Modules\Divisi\Tables;

use Laravolt\Suitable\Columns\Numbering;
use Laravolt\Suitable\Columns\RestfulButton;
use Laravolt\Suitable\Columns\Text;
use Laravolt\Suitable\TableView;
use Modules\Divisi\Models\Divisi;

class DivisiTableView extends TableView
{
    public function source()
    {
        return Divisi::autoSort()->latest()->autoSearch(request('search'))->paginate();
    }

    protected function columns()
    {
        return [
            Numbering::make('No'),
            Text::make('divisi')->sortable(),
            RestfulButton::make('modules::divisi')->except('view'),
        ];
    }
}
