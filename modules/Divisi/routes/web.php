<?php

use Modules\Divisi\Controllers\DivisiController;

$router->group(
    [
        'prefix' => config('modules.divisi.route.prefix'),
        'as' => 'modules::',
        'middleware' => config('modules.divisi.route.middleware'),
    ],
    function ($router) {
        $router->resource('divisi', DivisiController::class);
    }
);
