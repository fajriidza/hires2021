<?php

namespace Modules\Divisi\Controllers;

use Illuminate\Routing\Controller;
use Modules\Divisi\Requests\Store;
use Modules\Divisi\Requests\Update;
use Modules\Divisi\Models\Divisi;
use Modules\Divisi\Tables\DivisiTableView;

class DivisiController extends Controller
{
    public function index()
    {
        return DivisiTableView::make()->view('divisi::index');
    }

    public function create()
    {
        return view('divisi::create');
    }

    public function store(Store $request)
    {
        Divisi::create($request->validated());

        return redirect()->back()->withSuccess('Divisi saved');
    }

    public function show(Divisi $divisi)
    {
        return view('divisi::show', compact('divisi'));
    }

    public function edit(Divisi $divisi)
    {
        return view('divisi::edit', compact('divisi'));
    }

    public function update(Update $request, Divisi $divisi)
    {
        $divisi->update($request->validated());

        return redirect()->back()->withSuccess('Divisi saved');
    }

    public function destroy(Divisi $divisi)
    {
        $divisi->delete();

        return redirect()->back()->withSuccess('Divisi deleted');
    }
}
