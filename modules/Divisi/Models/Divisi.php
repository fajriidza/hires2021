<?php

namespace Modules\Divisi\Models;

use Illuminate\Database\Eloquent\Model;
use Laravolt\Support\Traits\AutoFilter;
use Laravolt\Support\Traits\AutoSearch;
use Laravolt\Support\Traits\AutoSort;

class Divisi extends Model
{
    use AutoSearch, AutoSort, AutoFilter;

    protected $table = 'divisi';

    protected $guarded = [];

    protected $searchableColumns = ["divisi",];
}
