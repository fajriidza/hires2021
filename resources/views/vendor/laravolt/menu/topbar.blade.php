<header class="ui menu borderless fixed top b-0">
    <div class="item mobile only tablet only" data-role="sidebar-visibility-switcher"><i class="icon sidebar"></i></div>

    @yield('page.back')

    <div class="menu" id="titlebar">
        <div class="item">
            <h2 class="ui header m-l-1">@yield('page.title')</h2>
        </div>

        @yield('page.actions')
    </div>

    <div class="menu right p-r-1" id="userbar" data-turbolinks-permanent>
        @auth
            <div class="item notif">
                <i class="large bell outline icon"></i>

{{--                @php($notificationsCount = Auth::user()->unreadNotifications->count())--}}
{{--                @if(!empty($notificationsCount))--}}
                    <div class="tiny floating ui circular label"
                         style="margin: 22px; color:white; background-color:red;">
{{--                        {{$notificationsCount}}--}}12
                    </div>
{{--                @endif--}}
            </div>
            <div class="ui fluid popup bottom left transition hidden pop" style="inset: 553px auto auto 1px; width: 400px !important; height: 500px;
    overflow-y: auto;">

{{--                @if(!empty($notificationsCount))--}}
                        @for($i=0;$i<10;$i++)
{{--                    @foreach(Auth::user()->unreadNotifications as $notification)--}}
                        <a class="item" href="">
{{--                            @php($message = $notification->data)--}}
                            <table>
                                <tr>
                                    <td rowspan=2><i class="bullhorn icon" aria-hidden="true"
                                                     style="color:black;"></i></td>
                                    <td>
                                        <h5 style="color:black;">Peringatan Habis Kontrak</h5>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p style="color:grey;">Pegawai Jhon akan habis kontrak dalam 7 hari</p>
                                    </td>
                                </tr>
                            </table>
                        </a>
                        <div class="divider"></div>
                @endfor
{{--                    @endforeach--}}
                    <div class="divider" style="margin:0px;"></div>
                    <a class="ui item" href="">
                        <x-button class="btn-allnotif" style="width:100%;" > Lihat Semua Notifikasi</x-button>
                    </a>
{{--                @else--}}
{{--                    <div class="item">--}}
{{--                        <table>--}}
{{--                            <tr>--}}
{{--                                <td><i class="bullhorn icon" aria-hidden="true" style="color:black;"></i></td>--}}
{{--                                <td><h5 style="color:black;">Tidak ada notifikasi baru.</h5></td>--}}
{{--                            </tr>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                    <div class="divider"></div>--}}
{{--                @endif--}}
            </div>
            <div class="item">
                <div class="ui compact menu b-0">
                    <div class="ui simple dropdown basic button top right pointing b-0 p-0">
                        <img src="{{ auth()->user()->avatar }}" alt="" class="ui image avatar">
                        <i class="dropdown icon m-l-0 {{ config('laravolt.ui.color') }}"></i>
                        <div class="menu">
                            <div class="header"><span class="ui text {{ config('laravolt.ui.color') }}">{{ auth()->user()->name }}</span></div>

                            <div class="divider"></div>

                            <a href="{{ route('epicentrum::my.profile.edit') }}" class="item">@lang('Edit Profil')</a>
                            <a href="{{ route('epicentrum::my.password.edit') }}" class="item">@lang('Edit Password')</a>

                            <div class="divider"></div>

                            <a href="{{ route('auth::logout') }}" class="item">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        @endauth

    </div>
</header>
@push('script')
    <script>
        $(function () {
            $('.notif')
                .popup({
                    popup : $('.pop'),
                    on    : 'click',
                    inline     : true,
                    lastResort: 'bottom right',
                    hoverable  : true,
                    setFluidWidth : false,
                    position   : 'bottom right',
                    delay: {
                        show: 300,
                        hide: 800
                    }
                })
            ;

            $(".close.icon").click(function () {
                $('.ui.sidebar')
                    .sidebar('hide');
            });
            $(".ui.menu .item.mobile.only.tablet.only .icon.sidebar").click(function () {
                $('.sidebar')
                    .addClass('show');
            });
        });
    </script>
@endpush
