@extends('laravolt::layouts.app')

@section('content')
    <div style="display: flex; min-height: 150px; align-items: center; justify-content: center; flex-direction: column">
        <h1 class="ui header" style="font-size: 5em; font-weight: 100; letter-spacing: .15em">
            Selamat Datang di HIRIS <br>
        </h1>
        <h2 class="ui header" style="letter-spacing: .15em">
            Sistem Informasi Data Pegawai
        </h2>
    </div>
    <br>
    <br>
    <div class="ui cards">
        @if($userRole->name === 'Admin')
            @foreach($roles as $role)
            <div class="card">
                <div class="content">
                    <div class="header"><h4>{{$role->name}}</h4></div>
                    <div class="description"><h2>
                            <i class="icon users large"></i>
                            {{$data[$role->name]}}
                        </h2>

                    </div>
                </div>
            </div>
            @endforeach
        @else
            <div class="card">
                <div class="content">
                    <div class="header"><h4>{{$userRole->name}}</h4></div>
                    <div class="description"><h2>
                            <i class="icon users large"></i>
                            {{$data[$userRole->name]}}
                        </h2>

                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
