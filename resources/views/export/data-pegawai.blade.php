<table>
    <thead>
    <tr>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
        <th style="text-align: center"></th>
    </tr>
    <tr>
        <th style="text-align: center"><b>No</b></th>
        <th style="text-align: center"><b>NIP</b></th>
        <th style="text-align: center"><b>NIK</b></th>
        <th style="text-align: center"><b>No BPJS Kesehatan</b></th>
        <th style="text-align: center"><b>No BPJS Tenaga Kerja</b></th>
        <th style="text-align: center"><b>Nama Karyawan</b></th>
        <th style="text-align: center"><b>Tempat Lahir</b></th>
        <th style="text-align: center"><b>Tanggal Lahir</b></th>
        <th style="text-align: center"><b>Pendidikan Terakhir</b></th>
        <th style="text-align: center"><b>Alamat</b></th>
        <th style="text-align: center"><b>Jabatan</b></th>
        <th style="text-align: center"><b>Divisi</b></th>
        <th style="text-align: center"><b>Perusahaan</b></th>
        <th style="text-align: center"><b>Tanggal Masuk</b></th>
        <th style="text-align: center"><b>Mulai Kontrak</b></th>
        <th style="text-align: center"><b>Habis Kontrak</b></th>
        <th style="text-align: center"><b>Catatan</b></th>
        <th style="text-align: center"><b>Keluarga</b></th>
        <th style="text-align: center"><b>Gaji</b></th>
        <th style="text-align: center"><b>Status</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $item)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->nip}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->no_bpjs_kesehatan}}</td>
            <td>{{$item->no_bpjs_tenaga_kerja}}</td>
            <td>{{$item->nama_karyawan}}</td>
            <td>{{$item->tempat_lahir}}</td>
            <td>{{format_date($item->tanggal_lahir)}}</td>
            <td>{{$item->pendidikan_terakhir}}</td>
            <td>{{$item->alamat}}</td>
            <td>{{$item->jabatan}}</td>
            <td>{{$item->divisi}}</td>
            <td>{{$item->perusahaan}}</td>
            <td>{{format_date($item->tanggal_masuk)}}</td>
            <td>{{format_date($item->mulai_kontrak)}}</td>
            <td>{{format_date($item->habis_kontrak)}}</td>
            <td>{{$item->catatan}}</td>
            <td>{{$item->keluarga}}</td>
            <td>{{format_gaji($item->gaji)}}</td>
            <td>{{$item->status}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
