<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti', function (Blueprint $table) {
            $table->id();
            $table->string('perusahaan')->nullable();
            $table->string('divisi')->nullable();
            $table->string('nama')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('jumlah_cuti')->nullable();
            $table->string('sisa_cuti')->nullable();
            $table->timestamp('tanggal_mulai_cuti')->nullable();
            $table->timestamp('tanggal_masuk')->nullable();
            $table->text('alasan')->nullable();
            $table->string('potong_cuti')->nullable();
            $table->string('tidak_potong_cuti')->nullable();
            $table->string('sisa_setelah_potong_cuti')->nullable();
            $table->string('potong_gaji')->nullable();
            $table->text('catatan');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuti');
    }
}
