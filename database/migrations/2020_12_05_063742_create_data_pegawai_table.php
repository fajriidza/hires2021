<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pegawai', function (Blueprint $table) {
            $table->id();
            $table->string('nip');
            $table->string('no_bpjs_tenaga_kerja');
            $table->string('no_bpjs_kesehatan');
            $table->string('nama_karyawan');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('pendidikan_terakhir');
            $table->text('alamat');
            $table->string('divisi');
            $table->string('perusahaan');
            $table->timestamp('tanggal_masuk');
            $table->timestamp('tanggal_masuk')->nullable();
            $table->timestamp('mulai_kontrak')->nullable();
            $table->timestamp('habis_kontrak')->nullable();
            $table->text('catatan');
            $table->text('keluarga');
            $table->string('gaji');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pegawai');
    }
}
