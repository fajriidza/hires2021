## Spesifikasi Minimal

1. PHP 7.3
2. MySQL v5.5


## Tools
1. Mac/Linux (diutamakan) / Windows
2. [Composer](https://getcomposer.org/download/)
4. [NPM](https://nodejs.org/en/download/)


## Tutorial

### Setup Project
1. Pastikan composer sudah terinstall, kemudian jalankan perintah:
    ```bash
    $ composer install
    ```

2. Setup environment. Copy (bukan rename) file `.env.example` menjadi `.env`.
Kemudian isikan config yang sesuai.

3. Jalankan perintah berikut untuk generate key.
    ```bash
    $ php artisan key:generate
    ```

4. Jalankan migrasi table dan seeder (data awal).
    ```bash
    $ php artisan migrate && php artisan seed
    ```

5. Install package untuk assets (package node_modules) dengan menggunakan perintah:
    ```bash
    $ npm install
    $ npm run dev
    ```
6. Jalankan perintah ini untuk membuat Admin akun
    ```
    $ php artisan laravolt:admin Admin admin@HRD.dev secret
    ```
7. Buka aplikasi pada url `http://localhost:8000` (sesuai port yang dipilih).

8. Happy coding :))
