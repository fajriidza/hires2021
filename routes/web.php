<?php

Route::get('/', function () {
    return redirect('/auth/login');
});
Route::get('/dashboard', 'Dashboard')->name('dashboard')->middleware('auth');
